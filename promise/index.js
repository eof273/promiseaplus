const stateSb = Symbol('Promise State')
const STATES = {
  /**
   * When pending, a promise:
   *  may transition to either the fulfilled or rejected state.
   */
  PENDING: 'pending',
  /**
   * When fulfilled, a promise:
   *  must not transition to any other state.
   *  must have a value, which must not change.
   */
  FULFILLED: 'fulfilled',
  /**
   * When rejected, a promise:
   *  must not transition to any other state.
   *  must have a reason, which must not change.
   */
  REJECTED: 'rejected',
}

function PromiseConstructor() {
  this[stateSb] = STATES.PENDING

}
