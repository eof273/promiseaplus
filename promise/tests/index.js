const {createTest} = require('../../testSystem')

;((PromiseConstructor) => {
  // 2.1.1.1
  createTest.todo('Pending: may transition to either the fulfilled or rejected state', (validator) => {})

  // 2.1.2.1
  createTest.todo('Fulfilled: must not transition to any other state')
  // 2.1.2.2
  createTest.todo('Fulfilled: must have a value, which must not change')

  // 2.1.3.1
  createTest.todo('Rejected: must not transition to any other state')
  // 2.1.3.2
  createTest.todo('Rejected: must have a reason, which must not change')

  // Here, "must not change" means immutable identity (i.e. ===), but does not imply deep immutability.

  // 2.2
  createTest('A promise must provide a then method to access its current or eventual value or reason', (validator) => {
    const fn = validator.createFn()
    const promise = new PromiseConstructor(fn, fn)

    validator.has(promise, 'then')
  })

  createTest('A promise’s then method accepts two arguments', (validator) => {
    const executor = validator.createFn('executor')
    const promise = new PromiseConstructor(executor)

    validator.is.equal(promise.then.length, 2)
  })

  // 2.2.1
  createTest.todo('Both onFulfilled and onRejected are optional arguments', (validator) => {})
  // 2.2.1.1
  createTest.todo('If onFulfilled is not a function, it must be ignored')
  // 2.2.1.2
  createTest.todo('If onRejected is not a function, it must be ignored')

  // 2.2.2 If onFulfilled is a function:
  // 2.2.2.1
  createTest.todo('it must be called after promise is fulfilled, with promise’s value as its first argument')
  // 2.2.2.2
  createTest.todo('it must not be called before promise is fulfilled')
  // 2.2.2.3
  createTest('it must not be called more than once', (validator) => {
    const executor = validator.createFn('executor')
    const onFulfilled = validator.createFn('onFulfilled')

    const promise = new PromiseConstructor(executor)
    promise.then(onFulfilled)

    validator.was.called(onFulfilled)
    validator.was.calledNTimes(onFulfilled, 1)

    promise.then()

    validator.was.calledNTimes(onFulfilled, 1)
  })

  // 2.2.3 If onRejected is a function:
  // 2.2.3.1
  createTest.todo('it must be called after promise is rejected, with promise’s reason as its first argument')
  // 2.2.3.2
  createTest.todo('it must not be called before promise is rejected')
  // 2.2.3.3
  createTest('it must not be called more than once', (validator) => {
    const executor = validator.createFn('executor')
    const onRejected = validator.createFn('onRejected')

    const promise = new PromiseConstructor(executor)
    promise.then(undefined, onRejected)

    validator.was.called(onRejected)
    validator.was.calledNTimes(onRejected, 1)

    promise.then()

    validator.was.calledNTimes(onRejected, 1)
  })

  // 2.2.4
  createTest.todo('onFulfilled or onRejected must not be called until the execution context stack contains only platform code. [3.1]')

  // 2.2.5
  createTest.todo('onFulfilled and onRejected must be called as functions (i.e. with no this value). [3.2]')

  // 2.2.6
  createTest.todo('then may be called multiple times on the same promise', (validator) => {
    const executor = validator.createFn('executor')

    const promise = new PromiseConstructor(executor)

    promise.then()
    promise.then()
    promise.then()
    // ???
  })

  // 2.2.6.1
  createTest('If/when promise is fulfilled, all respective onFulfilled callbacks must execute in the order of their originating calls to then', (validator) => {
    const executor = validator.createFn('executor')
    const onFulfilled1 = validator.createFn('onFulfilled1')
    const onFulfilled2 = validator.createFn('onFulfilled2')
    const onFulfilled3 = validator.createFn('onFulfilled3')

    const promise = new PromiseConstructor(executor)

    promise
      .then(onFulfilled1)
      .then(onFulfilled2)
      .then(onFulfilled3)

    validator.was.calledInOrder([
      onFulfilled1,
      onFulfilled2,
      onFulfilled3
    ])
  })
  // 2.2.6.2
  createTest.todo('If/when promise is rejected, all respective onRejected callbacks must execute in the order of their originating calls to then', (validator) => {
    // WHAT DA FUCK DOES IT MEANS ???
  })

  // 2.2.7
  createTest('then must return a promise [3.3]', (validator) => {
    const executor = validator.createFn('executor')
    const promise = PromiseConstructor(executor)

    validator.is.instance(promise, executor)
  })
})();
