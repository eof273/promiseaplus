const getOrder = (() => {
  let i = 0
  return () => i++
})()

module.exports = {getOrder}
