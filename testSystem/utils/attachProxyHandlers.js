const {notHandler, applyHandler} = require('./proxyHandlers')

const getHandlers = (validatorField) => {
  if (validatorField instanceof Function) {
    return {
      ...notHandler,
      ...applyHandler
    }
  }

  return notHandler
}

// @todo rename - it's literally "not" handlers
const attachProxyHandlers = (validator) => {
  Object.keys(validator).forEach((key) => {
    validator[key].not = new Proxy(validator[key], getHandlers(validator[key]))
  })
}

module.exports = {attachProxyHandlers}
