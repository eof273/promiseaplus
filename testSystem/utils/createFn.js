const {performance} = require('perf_hooks')

const {SYMBOLS} = require('../constants/symbols')
const {getOrder} = require('./getOrder')

const createFn = (name) => {
  const fn = () => {
    fn[SYMBOLS.CALLED] = true
    fn[SYMBOLS.FUNCTION_CALLS_COUNT] += 1
    fn[SYMBOLS.FUNCTION_EXECUTION_ORDER] = getOrder()
    // @todo add execution time tests for async functions
    fn[SYMBOLS.FUNCTION_EXECUTION_TIME] = performance.now()
  }
  fn[SYMBOLS.CALLED] = false
  fn[SYMBOLS.FUNCTION_CALLS_COUNT] = 0
  fn[SYMBOLS.FUNCTION_EXECUTION_ORDER] = 0
  fn[SYMBOLS.FUNCTION_EXECUTION_TIME] = 0
  fn[SYMBOLS.FUNCTION_NAME] = name

  return fn
}

module.exports = {createFn}
