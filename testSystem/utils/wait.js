const wait = (n) => new Promise((resolve) => setTimeout(resolve, n))

module.exports = {wait}
