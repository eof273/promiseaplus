const {PRINTER_COLORS} = require('../constants/printerColors')
const {FORMATTERS} = require('../constants/formatters')

const print = ({text, color, logger = console.log}) =>
  logger(color, text, '\n ')

// @todo check if error is our constant to separate with inner js errors
const printError = ({description, e, line}) => print({
  color: PRINTER_COLORS.ERROR,
  text: FORMATTERS.ERROR({description, e, line})
})

const printSuccess = ({description, line}) => print({
  color: PRINTER_COLORS.SUCCESS,
  text: FORMATTERS.SUCCESS({description, line})
})

const printTodo = ({description, line}) => print({
  color: PRINTER_COLORS.TODO,
  text: FORMATTERS.TODO({description, line})
})

module.exports = {
  print,
  printError,
  printSuccess,
  printTodo
}
