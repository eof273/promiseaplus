const getTestLine = () => new Error('').stack
  .split('\n')[3]
  .replace(/\s+/, '')
//.split(/\s+/)[2]

const getValidatorLine = (e) => e.stack
  .split('\n')
  .filter((s) =>
    s.includes(__filename)
  )[1]

module.exports = {
  getTestLine,
  getValidatorLine
}
