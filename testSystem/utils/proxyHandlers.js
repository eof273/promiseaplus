const {ERRORS} = require('../constants/errors')

const notHandler = {
  get(target, p, receiver) {
    return (...args) => {
      try {
        target[p](...args);
      } catch (e) {
        return
      }
      throw new Error(ERRORS.NOT(p))
    }
  }
}

const applyHandler = {
  apply(target, thisArg, argArray) {
    try {
      target(...argArray)
    } catch (e) {
      return
    }

    throw new Error(ERRORS.NOT(target.name))
  }
}

module.exports = {
  notHandler,
  applyHandler
}
