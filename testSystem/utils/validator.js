const {has} = require('../groups/has')
const {is} = require('../groups/is')
const {was} = require('../groups/was')

const {createFn} = require('./createFn')
const {wait} = require('./wait')

const {attachProxyHandlers} = require('../utils/attachProxyHandlers')

const validator = {
  has,
  is,
  was,
  // @todo should be somewhere else
  createFn,
  wait
}

attachProxyHandlers(validator)

module.exports = {validator}
