const {printSuccess, printError, printTodo} = require('./printers')
const {getTestLine} = require('./lines')
const {validator} = require('./validator')

const runSyncTest = ({description, testFn, line}) => {
  try {
    testFn(validator)
    printSuccess({description, line})
  } catch (e) {
    printError({description, e, line})
  }
}

const runAsyncTest = ({description, testFn, line}) => testFn(validator)
  .then(() => printSuccess({description, line}))
  .catch((e) => printError({description, e, line}))

const runTodo = (description, fn) => {
  printTodo({
    line: getTestLine(),
    description
  });
}

module.exports = {
  runSyncTest,
  runAsyncTest,
  runTodo
}
