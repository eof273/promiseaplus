const {NODE_COLORS} = require('../constants/nodeColors')

const createPrinterColor = (bg = NODE_COLORS.BgWhite, fg = NODE_COLORS.FgBlack) =>
  bg + fg + '%s' + NODE_COLORS.Reset

module.exports = {createPrinterColor}
