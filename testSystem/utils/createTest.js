const {types: {isAsyncFunction}} = require('util')
const {runAsyncTest, runSyncTest, runTodo} = require('./runners')
const {getTestLine} = require('./lines')

const createTest = (description, testFn) => {
  const testData = {
    line: getTestLine(),
    description,
    testFn
  }

  if (isAsyncFunction(testFn)) {
    runAsyncTest(testData)
  } else {
    runSyncTest(testData)
  }
  // @todo check if validator was not called
}
createTest.todo = runTodo

module.exports = {createTest}
