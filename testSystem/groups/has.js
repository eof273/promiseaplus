const {ERRORS} = require('../constants/errors')

const has = (object, field) => {
  if (!(field in object)) {
    throw new Error(ERRORS.HAS_NOT_PROPERTY(field))
  }
}

has.own = (object, field) => {
  if (!(object instanceof Object)) {
    throw new Error(ERRORS.NOT_AN_OBJECT(object))
  }

  if (!object.hasOwnProperty(field)) {
    throw new Error(ERRORS.HAS_NOT_OWN_PROPERTY(field))
  }
}

has.inPrototype = (object, field) => {
  if (!(object instanceof Object)) {
    throw new Error(ERRORS.NOT_AN_OBJECT(object))
  }

  if (object.hasOwnProperty(field)) {
    throw new Error(ERRORS.HAS_OWN_PROPERTY(field))
  }

  if (!(field in object)) {
    throw new Error(ERRORS.HAS_NOT_PROTOTYPE_PROPERTY(field))
  }
}

module.exports = {has}
