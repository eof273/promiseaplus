const {ERRORS} = require('../constants/errors')

const equal = (a, b) => {
  const aType = typeof a
  const bType = typeof b

  if (aType !== bType) {
    throw new Error(ERRORS.DIFFERENT_TYPES(a, aType, b, bType))
  }

  if ([typeof a, typeof b].every((type) => type === 'object')) {

  }

  if (a !== b) {
    throw new Error(ERRORS.NOT_EQUAL(a, b))
  }
}

const instance = (instance, constructor) => {
  if (!(instance instanceof constructor)) {
    throw new Error(ERRORS.NOT_AN_INSTANCE(instance, constructor))
  }
}

const is = {
  equal,
  instance
}

module.exports = {is}
