const {ERRORS} = require('../constants/errors')
const {SYMBOLS} = require('../constants/symbols')

const called = (fn) => {
  if(!fn[SYMBOLS.CALLED]) {
    throw new Error(ERRORS.WAS_NOT_CALLED(fn[SYMBOLS.FUNCTION_NAME]))
  }
}

const calledNTimes = (fn, n) => {
  const calls = fn[SYMBOLS.FUNCTION_CALLS_COUNT]
  if (n !== calls) {
    throw new Error(ERRORS.FUNCTION_WAS_CALLED_WRONG_NUMBER_OF_TIMES(n, calls))
  }
}

const calledInOrder = (fns) => {
  let previousTs = 0
  let currentTs = 0

  let previousFn = ''
  let currentFn = ''

  for (let i = 0, l = fns.length; i < l; i++) {
    const fn = fns[i]
    currentTs = fn[SYMBOLS.FUNCTION_EXECUTION_ORDER]
    currentFn = fn[SYMBOLS.FUNCTION_NAME]

    if (previousTs > currentTs) {
      throw new Error(ERRORS.CALLED_NOT_IN_ORDER(previousFn, currentFn))
    }

    previousTs = currentTs
    previousFn = currentFn
  }
}

const was = {
  called,
  calledNTimes,
  calledInOrder
}

module.exports = {was}
