const {getValidatorLine} = require('../utils/lines')

const FORMATTERS = {
  ERROR: ({description, e, line}) =>
    `FAILED: "${description}"\n${line}
    ${e.message}
    ${getValidatorLine(e)}`,
  SUCCESS: ({description, line}) => `PASSED: "${description}"\n${line}`,
  TODO: ({description, line}) => `TODO: "${description}"\n${line}`
}

module.exports = {FORMATTERS}
