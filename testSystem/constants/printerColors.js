const {NODE_COLORS} = require('./nodeColors')
const {createPrinterColor} = require('../utils/createPrinterColor')

const PRINTER_COLORS = {
  ERROR: createPrinterColor(NODE_COLORS.BgRed),
  SUCCESS: createPrinterColor(NODE_COLORS.BgGreen),
  TODO: createPrinterColor(NODE_COLORS.BgBlue)
}

module.exports = {PRINTER_COLORS}
