const SYMBOLS = {
  CALLED: Symbol('Is function was called'),
  FUNCTION_NAME: Symbol('Function name'),
  FUNCTION_CALLS_COUNT: Symbol('Function calls count'),
  FUNCTION_EXECUTION_ORDER: Symbol('Function execution order'),
  FUNCTION_EXECUTION_TIME: Symbol('Function execution time'),
}

module.exports = {SYMBOLS}
