const ERRORS = {
  DIFFERENT_TYPES: (a, aT, b, bT) => `Arguments has different types: (${aT}) (${a}) and (${bT}) (${b})`,
  NOT_EQUAL: (a, b) => `Arguments are not equal: (${a}) and (${b})`,
  WAS_NOT_CALLED: (fn) => `Function was not called: (${fn})`,
  NOT: (p) => `"Not" condition failed for "${p}"`,
  HAS_NOT_OWN_PROPERTY: (field) => `Has not own property (${field})`,
  HAS_OWN_PROPERTY: (field) => `Has own property (${field})`,
  HAS_NOT_PROTOTYPE_PROPERTY: (field) => `Has not prototype property (${field})`,
  HAS_NOT_PROPERTY: (field) => `Has not property (${field})`,
  NOT_AN_OBJECT: (smth) => `Is not an object: (${smth})`,
  FUNCTION_WAS_CALLED_WRONG_NUMBER_OF_TIMES: (n, calls) => `Function was called wrong number of times (${calls}) - expected (${n})`,
  CALLED_NOT_IN_ORDER: (fnA, fnB) => `Called in wrong order: (${fnA}) was called earlier than (${fnB})`,
  NOT_AN_INSTANCE: (instance, constructor) => `(${instance}) is not an instance on (${constructor.name || constructor})`
}

module.exports = {ERRORS}
