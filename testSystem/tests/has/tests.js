const {createTest} = require('../../')

;(() => {
  createTest('FAILED: has', (validator) => {
    const object = {}

    validator.has(object, 'a')
  })

  createTest('PASSED: has', (validator) => {
    const prototype = {a: 13}
    const object = Object.create(prototype)
    object.b = 666
    const symbol = Symbol('Symbol property')
    object[symbol] = 4

    validator.has(object, 'a')
    validator.has(object, 'b')
    validator.has(object, symbol)
  })

  createTest('FAILED: NOT has', (validator) => {
    const object = {a: 13}

    validator.has.not(object, 'a')
  })

  createTest('PASSED: NOT has', (validator) => {
    const object = {}

    validator.has.not(object, 'a')
  })

  createTest('FAILED: has own', (validator) => {
    const prototype = {a: 13}
    const object = Object.create(prototype)

    // @todo All failed tests should be separated T-T
    validator.has.own(object, 'a')
    validator.has.own(object, 'b')
  })

  createTest('PASSED: has own', (validator) => {
    const object = {a: 13}

    validator.has.own(object, 'a')
  })

  createTest('FAILED: NOT has own', (validator) => {
    const object = {a: 13}

    validator.has.not.own(object, 'a')
  })

  createTest('PASSED: NOT has own', (validator) => {
    const prototype = {a: 13}
    const object = Object.create(prototype)

    validator.has.not.own(object, 'a')
    validator.has.not.own(object, 'b')
  })

  createTest('FAILED: has in prototype', (validator) => {
    const prototype = {a: 12}
    const object = Object.create(prototype)
    object.b = 666

    validator.has.inPrototype(object, 'b')
    validator.has.inPrototype(object, 'c')
  })

  createTest('PASSED: has in prototype', (validator) => {
    const prototype = {a: 13}
    const object = Object.create(prototype)

    validator.has.inPrototype(object, 'a')
  })

  createTest('FAILED: NOT has in prototype', (validator) => {
    const prototype = {a: 13}
    const object = Object.create(prototype)

    validator.has.not.inPrototype(object, 'a')
  })

  createTest('PASSED: NOT has in prototype', (validator) => {
    const object = {a: 13}

    validator.has.not.inPrototype(object, 'a')
    validator.has.not.inPrototype(object, 'b')
  })
})()
