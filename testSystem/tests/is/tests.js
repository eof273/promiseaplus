const {createTest} = require('../../')

;(() => {
  createTest.todo('is.instance')

  createTest('FAILED: equal', (validator) => {
    validator.is.equal(12, '12')
  })

  createTest('PASSED: equal', (validator) => {
    validator.is.equal(36, 36)
  })

  createTest('FAILED: NOT equal', (validator) => {
    validator.is.not.equal(12, 12)
  })

  createTest('PASSED: NOT equal', (validator) => {
    validator.is.not.equal(36, '36')
  })
})()
