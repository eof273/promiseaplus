const {createTest} = require('../../')

;(() => {
  createTest.todo('was.not.calledNTimes')

  createTest('FAILED: called', (validator) => {
    const testFn = validator.createFn('not called function')

    validator.was.called(testFn)
  })

  createTest('PASSED: called', (validator) => {
    const testFn = validator.createFn('called function')
    testFn()

    validator.was.called(testFn)
  })

  createTest('FAILED: NOT called', (validator) => {
    const testFn = validator.createFn('called function')
    testFn()

    validator.was.not.called(testFn)
  })

  createTest('PASSED: NOT called', (validator) => {
    const testFn = validator.createFn('not called function')

    validator.was.not.called(testFn)
  })

  createTest('FAILED: called in order', async (validator) => {
    const fn1 = validator.createFn('first')
    const fn2 = validator.createFn('second')
    const fn3 = validator.createFn('third')

    fn1()
    await validator.wait(10)
    fn2()
    await validator.wait(10)
    fn3()

    validator.was.calledInOrder([fn2, fn3, fn1])
  })

  createTest('PASSED: called in order', async (validator) => {
    const fn1 = validator.createFn('first')
    const fn2 = validator.createFn('second')
    const fn3 = validator.createFn('third')
    const fn4 = validator.createFn('fourth')
    const fn5 = validator.createFn('fifth')

    fn1()
    await validator.wait(10)
    fn2()
    await validator.wait(10)
    fn3()
    await validator.wait(10)
    fn4()
    await validator.wait(10)
    fn5()

    validator.was.calledInOrder([fn1, fn2, fn3, fn4, fn5])
  })

  createTest('FAILED: NOT called in order', async (validator) => {
    const fn1 = validator.createFn('firstFailed')
    const fn2 = validator.createFn('secondPassed')
    const fn3 = validator.createFn('thirdPassed')

    fn1()
    await validator.wait(10)
    fn2()
    await validator.wait(10)
    fn3()

    validator.was.not.calledInOrder([fn1, fn2, fn3])
  })

  createTest('PASSED: NOT called in order', async (validator) => {
    const fn1 = validator.createFn('firstFailed')
    const fn2 = validator.createFn('secondFailed')
    const fn3 = validator.createFn('thirdFailed')

    fn1()
    await validator.wait(10)
    fn2()
    await validator.wait(10)
    fn3()

    validator.was.not.calledInOrder([fn3, fn1, fn2])
  })
})()
