const {createTest} = require('../../')

;(() => {
  createTest.todo('TODO: Description')

  const PROMISE_VALUE = Math.random()
  const p = new Promise((resolve, reject) => setTimeout(() => resolve(PROMISE_VALUE), 1000))

  createTest('FAILED: Test async', async (validator) => {
    const promiseResult = await p

    validator.is.not.equal(promiseResult, PROMISE_VALUE)
    validator.is.equal(promiseResult, void 0)
  })

  createTest('PASSED: Test async', async (validator) => {
    const promiseResult = await p

    validator.is.equal(promiseResult, PROMISE_VALUE)
    validator.is.not.equal(promiseResult, '')
  })
})()
